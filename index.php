<HEAD>
<link href="style.css" rel="stylesheet" type="text/css">
<title>Homepage</title>
<link rel="icon" href="./assets/favicon.ico">
</HEAD>
<BODY>
	<div class="head">
<h1 style="padding-left: 40; float: left;">Homepage</h1>
	</div>
	
	<div class="staticFollower">
		<div class="cell">
				<a href="http://campus.belgrano.ort.edu.ar/tic/2016-br5a"><h2 style="padding-left: 40;">Repository</h2></a>
		</div>
		<div class="cell">
				<a href="http://campus.belgrano.ort.edu.ar/tic/2016-br5a"><h2 style="padding-left: 40;">Contact Info</h2></a>
		</div>
		<div class="cell">
				<a href="http://campus.belgrano.ort.edu.ar/tic/2016-br5a"><h2 style="padding-left: 40;">Option</h2></a>
		</div>
	</div>
	
	<div style="height: 20%;">
	</div>
	<div class="follower">
		<div class="cell">
				<a href="https://bitbucket.org/1mancrusade/homepage"><h2 style="padding-left: 40;">Repository</h2></a>
		</div>
		<div class="cell">
				<a href="https://bitbucket.org/1mancrusade/"><h2 style="padding-left: 40;">Contact Info</h2></a>
		</div>
		<div class="cell">
				<a href="http://campus.belgrano.ort.edu.ar/tic/2016-br5a"><h2 style="padding-left: 40;">Option</h2></a>
		</div>
	</div>
	<!--
	<div class="table" style="background-color: #38d0f0; border-bottom: 1px solid #38d0f0;">
		<h2 style="padding-left: 40;">Search Engines</h2>
	</div>
	<div class="table" style="border-top: 1px solid #38d0f0;">
		<div class="cell">
			<ul>
			<form id="tfnewsearch" method="get" action="https://www.startpage.com/do/search">
				<input type="text" name="q" style="width:100%;" autofocus>
				<input type="submit" value ="Startpage" style="width:50%;">
			</form>
			<form id="tfnewsearch" method="get" action="https://www.google.com/search">
				<input type="text" name="q" style="width:100%;">
				<input type="submit" value ="Google" style="width:50%;">
			</form>
			<ul>
		</div>
	</div>
	
	<br>
	-->
	<div class="table" style="background-color: #38d0f0; border-bottom: 1px solid #38d0f0;">
		<h2 style="padding-left: 40;">Bookmarks</h2>
	</div>
	<div class="table" style="border-top: 1px solid #38d0f0;">
		<div class="cell">
			<ul>
				<h3>Main</h3>
				<li><a href="http://campus.belgrano.ort.edu.ar/tic/2016-br5a">BR5A</a></li>
				<li><a href="http://drive.google.com/">Drive</a></li>
				<li><a href="https://tutanota.com/">Tutanota</a></li>
				<li><a href="http://bitbucket.org/1mancrusade/">Bitbucket</a></li>
				<li><a href="http://gmail.com/">Gmail</a></li>
				<li><a href="https://web.whatsapp.com/">WhatsApp Web</a></li>
			</ul>
		</div>
		<div class="cell">
			<ul>
				<h3>Social/Media</h3>
				<li><a href="https://www.stormfront.org/forum/">Stormfront</a></li>
				<li><a href="http://smashboards.com/">Smashboards</a></li>
				<li><a href="http://youtube.com/">Youtube</a></li>
				<li><a href="http://www.emuparadise.me/">Emuparadise</a></li>
				<li><a href="http://reddit.com/">Reddit</a></li>
				<li><a href="http://twitter.com/">Twitter</a></li>
				<li><a href="http://plays.tv/">Plays</a></li>
				
			</ul>
		</div>
		<div class="cell">
			<ul>
				<h3>Torrents</h3>
				<li><a href="http://www.ahashare.com/">Ahashare</a></li>
				<li><a href="http://nyaa.se/">Nyaa</a></li>
				<li><a href="https://tpb.run/">Retro Piratebay</a></li>
				<li><a href="https://proxybay.tv/">Proxybay</a></li>
			</ul>
		</div>
		<div class="cell">
			<ul>
				<h3>Tools</h3>
				<li><a href="http://www.clipconverter.cc/">Converter</a></li>
				<li><a href="https://pcpartpicker.com/">PCPartpicker</a></li>
				<li><a href="http://c9.io">Cloud 9</a></li>
			</ul>
		</div>
		<div class="cell">
			<ul>
				<h3>Musica</h3>
				<li><a href="http://technicaldeathmetal.org/category/instrumental-metal/">Technical death metal</a></li>
				<li><a href="http://www.progrockandmetal.net/">Prog Rock and Metal</a></li>
				<li><a href="http://www.ninsheetmusic.org/">Nintendo Musicsheets</a></li>
			</ul>
		</div>
	</div>
	
	<br>
	
	<div class="table" style="background-color: #38d0f0; border-bottom: 1px solid #38d0f0;">
		<a href="http://4chan.org"><h2 style="padding-left: 40;">Imageboards</h2></a>
	</div>
	<div class="table" style="border-top: 1px solid #38d0f0;">
		<div class="cell">
			<ul>
				<h3>4Chan</h3>
				<li><a href="http://boards.4chan.org/vr/">Retro Games</a></li>
				<li><a href="http://boards.4chan.org/g/">Technology</a></li>
				<li><a href="http://boards.4chan.org/k/">Weapons</a></li>
				<li><a href="http://boards.4chan.org/sci/">Science &amp; Math</a></li>
				<li><a href="http://boards.4chan.org/mu/">Music</a></li>
				<li><a href="http://boards.4chan.org/qst/">Quests</a></li>
				<li><a href="http://boards.4chan.org/fit/">Fitness</a></li>
				<li><a href="http://boards.4chan.org/pol/">Politically Incorrect</a></li>
				<li><a href="http://boards.4chan.org/e/">Ecchi</a></li>
				<li><a href="http://boards.4chan.org/t/">Torrents</a></li>
			</ul>
		</div>
		<div class="cell">
			<ul>
				<h3>8Chan</h3>
				<li><a href="http://8ch.net/argentina/">Argentina</a></li>
				<li><a href="http://8ch.net/pol/">Pollitically Incorrect</a></li>
			</ul>
		</div>
		<div class="cell">
			<ul>
				<h3>Endchan</h3>
				<li><a href="https://endchan.xyz/ggrevolt/">GG Revolt</a></li>
			</ul>
		</div>
	</div>
	
<br>

<div class="foot">
	<p style="font-size: 12px">Ramiro Basile 5to TIC A, 20/06/2016</p>
</div>
</BODY>
</HTML>